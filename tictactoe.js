const table = document.getElementById("table");

let nextPlayer = "playerUm";

let um = [
    [0 , 0 , 0],
    [0 , 0 , 0],
    [0 , 0,  0]
]
let dois = [
    [0 , 0 , 0],
    [0 , 0 , 0],
    [0 , 0,  0]
]

// Os aux servem pra limitar cada celula a uma jogada
let aux1 = 1;
let aux2 = 1;
let aux3 = 1;
let aux4 = 1;
let aux5 = 1;
let aux6 = 1;
let aux7 = 1;
let aux8 = 1;
let aux9 = 1;
table.addEventListener("click", function(e) {
    if(e.target.id === "box1") {
        if (aux1 === 1) {
            if (nextPlayer === "playerUm") {
                let box1 = document.getElementById("box1")
                let X = document.createElement("div");
                X.classList.add("xis");
                X.textContent = "X";
                box1.appendChild(X);
                nextPlayer = "PlayerDois"
                um[0][0] = 1;
        } else if (nextPlayer === "PlayerDois") {
            let box1 = document.getElementById("box1")
            let O = document.createElement("div");
            O.textContent = "O";
            O.classList.add("bola");
            box1.appendChild(O)
            nextPlayer = "playerUm";
            dois[0][0] = 1;
        }
        }
        aux1 = 0;
    } else if(e.target.id === "box2") {
        if(aux2 === 1) {
           if (nextPlayer === "playerUm") {
            let box2 = document.getElementById("box2")
            let X = document.createElement("div");
            X.classList.add("xis");
            X.textContent = "X";
            box2.appendChild(X)
            nextPlayer = "PlayerDois";
            um[0][1] = 1;
        } else if (nextPlayer === "PlayerDois") {
            let box2 = document.getElementById("box2")
            let O = document.createElement("div");
            O.textContent = "O";
            O.classList.add("bola");
            box2.appendChild(O)
            nextPlayer = "playerUm";
            dois[0][1] = 1;
        } 
        }
        aux2 = 0;
    } else if(e.target.id === "box3") {
        if(aux3 === 1) {
            if (nextPlayer === "playerUm") {
                let box3 = document.getElementById("box3")
                let X = document.createElement("div");
                X.classList.add("xis");
                X.textContent = "X";
                box3.appendChild(X);
                nextPlayer = "PlayerDois";
                um[0][2] = 1;
            } else if (nextPlayer === "PlayerDois") {
                let box3 = document.getElementById("box3")
                let O = document.createElement("div");
                O.textContent = "O";
                O.classList.add("bola");
                box3.appendChild(O)
                nextPlayer = "playerUm";
                dois[0][2] = 1;
        }
        }
        aux3 = 0;
    } else if(e.target.id === "box4") {
        if(aux4 === 1) {
            if (nextPlayer === "playerUm") {
                let box4 = document.getElementById("box4")
                let X = document.createElement("div");
                X.textContent = "X";
                X.classList.add("xis");
                box4.appendChild(X)
                nextPlayer = "PlayerDois"
                um[1][0] = 1;
            } else if (nextPlayer === "PlayerDois") {
                let box4 = document.getElementById("box4")
                let O = document.createElement("div");
                O.textContent = "O";
                O.classList.add("bola");
                box4.appendChild(O)
                nextPlayer = "playerUm";
                dois[1][0] = 1;
            }
        }
        aux4 = 0;
    } else if(e.target.id === "box5") {
        if( aux5 === 1) {
            if (nextPlayer === "playerUm") {
                let box5 = document.getElementById("box5")
                let X = document.createElement("div");
                X.textContent = "X";
                X.classList.add("xis");
                box5.appendChild(X)
                nextPlayer = "PlayerDois"
                um[1][1] = 1;
            } else if (nextPlayer === "PlayerDois") {
                let box5 = document.getElementById("box5")
                let O = document.createElement("div");
                O.textContent = "O";
                O.classList.add("bola");
                box5.appendChild(O)
                nextPlayer = "playerUm";
                dois[1][1] = 1;
            }
        }
        aux5 = 0;
    } else  if(e.target.id === "box6") {
        if (aux6 === 1) {
            if (nextPlayer === "playerUm") {
                let box6 = document.getElementById("box6")
                let X = document.createElement("div");
                X.textContent = "X";
                X.classList.add("xis");
                box6.appendChild(X);
                nextPlayer = "PlayerDois";
                um[1][2] = 1;
            } else if (nextPlayer === "PlayerDois") {
                let box6 = document.getElementById("box6")
                let O = document.createElement("div");
                O.textContent = "O";
                O.classList.add("bola");
                box6.appendChild(O)
                nextPlayer = "playerUm";
                dois[1][2] = 1;
            }
        }
        aux6 = 0;
    } else if(e.target.id === "box7") {
        if(aux7 === 1) {
            if (nextPlayer === "playerUm") {
                let box7 = document.getElementById("box7")
                let X = document.createElement("div");
                X.textContent = "X";
                X.classList.add("xis");
                box7.appendChild(X)
                nextPlayer = "PlayerDois"
                um[2][0] = 1;
            } else if (nextPlayer === "PlayerDois") {
                let box7 = document.getElementById("box7")
                let O = document.createElement("div");
                O.textContent = "O";
                O.classList.add("bola");
                box7.appendChild(O)
                nextPlayer = "playerUm";
                dois[2][0] = 1;
            }
        }
        aux7 = 0;
    } else if(e.target.id === "box8") {
        if(aux8 === 1) {
            if (nextPlayer === "playerUm") {
                let box8 = document.getElementById("box8")
                let X = document.createElement("div");
                X.textContent = "X";
                X.classList.add("xis");
                box8.appendChild(X)
                nextPlayer = "PlayerDois"
                um[2][1] = 1;
            } else if (nextPlayer === "PlayerDois") {
                let box8 = document.getElementById("box8")
                let O = document.createElement("div");
                O.textContent = "O";
                O.classList.add("bola");
                box8.appendChild(O)
                nextPlayer = "playerUm";
                dois[2][1] = 1;
            }
        }
        aux8 = 0;
    } else if(e.target.id === "box9") {
        if(aux9 === 1) {
            if (nextPlayer === "playerUm") {
                let box9 = document.getElementById("box9")
                let X = document.createElement("div");
                X.textContent = "X";
                X.classList.add("xis");
                box9.appendChild(X)
                nextPlayer = "PlayerDois"
                um[2][2] = 1;
            } else if (nextPlayer === "PlayerDois") {
                let box9 = document.getElementById("box9")
                let O = document.createElement("div");
                O.textContent = "O";
                O.classList.add("bola");
                box9.appendChild(O)
                nextPlayer = "playerUm";
                dois[2][2] = 1;
            }
        }
     aux9 = 0;
    }

    let soma = 0;
    for ( let i = 0 ; i < um.length ; i++)
        for ( let j = 0 ; j < um[i].length ; j++)
            soma += um[i][j];

    for (  i = 0 ; i < dois.length ; i++)
        for (  j = 0 ; j < dois[i].length ; j++)
            soma += dois[i][j];

    if ( um[0][0] + um[0][1] + um[0][2] === 3 )
        alert("Um ganhou")
    else if ( um[1][0] + um[1][1] + um[1][2] === 3 )
        alert("Um ganhou")
    else if ( um[2][0] + um[2][1] + um[2][2] === 3 )
        alert("Um ganhou")
    else if ( um[0][0] + um[1][0] + um[2][0] === 3 )
        alert("Um ganhou")
    else if ( um[0][1] + um[1][1] + um[2][1] === 3 )
        alert("Um ganhou")
    else if ( um[0][2] + um[1][2] + um[2][2] === 3 )
        alert("Um ganhou")
    else if (um[0][0] + um [1][1] + um [2][2] === 3)
        alert("Um ganhou")
    else if (um[0][2] + um[1][1] + um [2][0] === 3)
        alert("Um ganhou")


    else if ( dois[0][0] + dois[0][1] + dois[0][2] === 3 )
        alert("Dois ganhou")
    else if ( dois[1][0] + dois[1][1] + dois[1][2] === 3 )
        alert("Dois ganhou")
    else if ( dois[2][0] + dois[2][1] + dois[2][2] === 3 )
        alert("Dois ganhou")
    else if ( dois[0][0] + dois[1][0] + dois[2][0] === 3 )
        alert("Dois ganhou")
    else if ( dois[0][1] + dois[1][1] + dois[2][1] === 3 )
        alert("Dois ganhou")
    else if ( dois[0][2] + dois[1][2] + dois[2][2] === 3 )
        alert("Dois ganhou")
    else if (dois[0][0] + dois [1][1] + dois [2][2] === 3)
        alert("Dois ganhou")
    else if (dois[0][2] + dois[1][1] + dois [2][0] === 3)
        alert("Dois ganhou")
    else if (soma === 9) {
        alert("Deu velha");
    }
    

});